package littlecrow.dinerus;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import littlecrow.dinerus.data.AccessData;
import littlecrow.dinerus.data.CurrencyEnum;
import littlecrow.dinerus.data.DinerusCollection;
import littlecrow.dinerus.data.DinerusData;
import littlecrow.dinerus.data.DinerusObject;

public class DinerusReport extends Fragment {

    TextView textin;
    LinearLayout reportin;

    public DinerusReport() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.dinerus_report, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {

        textin = view.findViewById(R.id.textin);
        reportin = view.findViewById(R.id.reportin);
        updateReport();
//Aca poner cositas
    }

    public void updateReport(){
        reportin.removeAllViews();
        buildReport(AccessData.getTotalData(), reportin);
    }

    public void buildReport(DinerusObject dinerusObject, LinearLayout linearLayout){
        if(dinerusObject instanceof DinerusCollection){
            buildReport((DinerusCollection) dinerusObject, linearLayout);
        } else if (dinerusObject instanceof DinerusData){
            buildReport((DinerusData) dinerusObject, linearLayout);
        }
    }
    public void buildReport(DinerusCollection dinerusCollection, LinearLayout linearLayout){
        List<DinerusObject> dinerusObjectList = dinerusCollection.getDinerusObjectList();

        TextView name = new TextView(getContext());
        name.setText(dinerusCollection.getIdentifier().toString());
        linearLayout.addView(name);
        for (int i = 0; i < dinerusObjectList.size(); i++) {
            DinerusObject dinerusObject = dinerusObjectList.get(i);
            buildReport(dinerusObject,linearLayout);
        }
    }

    public void buildReport(DinerusData dinerusData, LinearLayout linearLayout){
        TextView amount = new TextView(getContext());
        amount.setText(dinerusData.getDinerusAmountDetail(CurrencyEnum.ARS).getDinerusAmount().toString());
        linearLayout.addView(amount);
    }

}
