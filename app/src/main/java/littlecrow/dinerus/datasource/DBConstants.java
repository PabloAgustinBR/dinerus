package littlecrow.dinerus.datasource;

public class DBConstants {

    public static class Data {

        public static final String dinerusTable = "dinerus";
        public static final String dinerusId = "id_dinerus";
        public static final String dinerusDate = "dinerus_date";
        public static final String dinerusCategory =  "dinerus_category";
        public static final String dinerusWallet =  "dinerus_wallet";
        public static final String dinerusDetail =  "dinerus_detail";
        public static final String dinerusAmountDetail =  "dinerus_amount_detail";
        public static final String dinerusImage =  "dinerus_image";


        public static final String categoryTable = "category";
        public static final String categoryId = "id_category";
        public static final String categoryName = "category_name";
        public static final String categoryDescription =  "category_description";
        public static final String categoryImage =  "category_image";

        public static final String walletTable = "wallet";
        public static final String walletId = "id_wallet";
        public static final String walletName = "wallet_name";
        public static final String walletDescription =  "wallet_description";
        public static final String walletImage =  "wallet_image";
    }

    public static class Tag {

        public static final String dinerusId = "$dinerus_id";
        public static final String dinerusDate = "$dinerus_date";
        public static final String dinerusCategory = "$dinerus_category";
        public static final String dinerusWallet =  "$dinerus_wallet";
        public static final String dinerusDetail = "$dinerus_detail";
        public static final String dinerusAmountDetail = "$dinerus_amount_detail";


        public static final String categoryId = "$id_category";
        public static final String categoryName = "$category_name";
        public static final String categoryDescription =  "$category_description";

        public static final String walletId = "$id_wallet";
        public static final String walletName = "$wallet_name";
        public static final String walletDescription =  "$wallet_description";

    }

    public static class Queries {

        public static final String QUERY_DINERUS_SELECT_ALL = "select " +
                Data.dinerusId + " , " +
                Data.dinerusDate + " , " +
                Data.dinerusCategory + " , " +
                Data.dinerusWallet + " , " +
                Data.dinerusDetail + " , " +
                Data.dinerusAmountDetail +
                " from " + Data.dinerusTable +
                " order by " +
                Data.dinerusDate + " asc ";

        public static final String QUERY_DINERUS_INSERT_ONE = "insert " +
                " into " + Data.dinerusTable + " ( " +
                Data.dinerusDate + " , " +
                Data.dinerusCategory + " , " +
                Data.dinerusWallet + " , " +
                Data.dinerusDetail + " , " +
                Data.dinerusAmountDetail +
                " ) " +
                " values " + " ( " +
                Tag.dinerusDate + " , " +
                Tag.dinerusCategory  + " , " +
                Tag.dinerusWallet  + " , " +
                Tag.dinerusDetail  + " , " +
                Tag.dinerusAmountDetail +
                " ) ";

        public static final String QUERY_DINERUS_UPDATE_ONE = "update " +
                Data.dinerusTable +
                " set " +
                Data.dinerusDate + " = " + Tag.dinerusDate + " , " +
                Data.dinerusCategory + " = " + Tag.dinerusCategory + " , " +
                Data.dinerusWallet + " = " + Tag.dinerusWallet + " , " +
                Data.dinerusDetail + " = " + Tag.dinerusDetail + " , " +
                Data.dinerusAmountDetail + " = " + Tag.dinerusAmountDetail +
                " where " +
                Data.dinerusId + " = " + Tag.dinerusId;

        public static final String QUERY_DINERUS_REMOVE_ONE = "remove " +
                " from " + Data.dinerusTable +
                " where " +
                Data.dinerusId + " = " +
                Tag.dinerusId;


        public static final String QUERY_CATEGORY_SELECT_ALL = "select " +
                Data.categoryId + " , " +
                Data.categoryName + " , " +
                Data.categoryDescription +
                " from " + Data.categoryTable;

        public static final String QUERY_CATEGORY_INSERT_ONE = "insert " +
                " into " + Data.categoryTable + " ( " +
                Data.categoryName + " , " +
                Data.categoryDescription +
                " ) " +
                " values " + " ( " +
                Tag.categoryName + " , " +
                Tag.categoryDescription +
                " ) ";

        public static final String QUERY_CATEGORY_UPDATE_ONE = "update " +
                Data.categoryTable +
                " set " +
                Data.categoryName + " = " + Tag.categoryName + " , " +
                Data.categoryDescription + " = " + Tag.categoryDescription +
                " where " +
                Data.dinerusId + " = " + Tag.dinerusId;

        public static final String QUERY_CATEGORY_REMOVE_ONE = "remove " +
                " from " + Data.categoryTable +
                " where " +
                Data.categoryId + " = " +
                Tag.categoryId;


        public static final String QUERY_WALLET_SELECT_ALL = "select " +
                Data.walletId + " , " +
                Data.walletName + " , " +
                Data.walletDescription +
                " from " + Data.walletTable;

        public static final String QUERY_WALLET_INSERT_ONE = "insert " +
                " into " + Data.walletTable + " ( " +
                Data.walletName + " , " +
                Data.walletDescription +
                " ) " +
                " values " + " ( " +
                Tag.walletName + " , " +
                Tag.walletDescription +
                " ) ";

        public static final String QUERY_WALLET_UPDATE_ONE = "update " +
                Data.walletTable +
                " set " +
                Data.walletName + " = " + Tag.walletName + " , " +
                Data.walletDescription + " = " + Tag.walletDescription +
                " where " +
                Data.walletId + " = " + Tag.walletId;

        public static final String QUERY_WALLET_REMOVE_ONE = "remove " +
                " from " + Data.walletTable +
                " where " +
                Data.walletId + " = " +
                Tag.walletId;
    }
}
