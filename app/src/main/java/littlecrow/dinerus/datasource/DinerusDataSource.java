package littlecrow.dinerus.datasource;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;

import java.io.IOException;

import littlecrow.dinerus.data.CategoryData;
import littlecrow.dinerus.data.DateUtils;
import littlecrow.dinerus.data.DinerusData;
import littlecrow.dinerus.data.WalletData;

public enum DinerusDataSource {

    INSTANCE;
    private SQLiteDatabase database;
    private boolean isDbClosed =true;
    private AdminSQLiteOpenHelper adminOpenHelper;
    


    public void init(Context context) throws SQLException, IOException {
        adminOpenHelper= new AdminSQLiteOpenHelper(context);
        if(isDbClosed){
            isDbClosed =false;

            adminOpenHelper.createDataBase();

            database = adminOpenHelper.getWritableDatabase();

        }

    }

    public boolean necessaryUpdate(){

        return adminOpenHelper.getRequiredUpdate();

    }

    public void updateDB(){

        closeDatabase();

        try {
            adminOpenHelper.createDataBase();
        }catch (IOException e) {
            e.printStackTrace();
        }

        database = adminOpenHelper.getWritableDatabase();

    }

    public boolean isDatabaseClosed(){
        return isDbClosed;
    }

    public void closeDatabase(){
        if(!isDbClosed && database != null){
            isDbClosed =true;
            database.close();
        }
    }

    public Cursor runRawQuery(String query){
        return database.rawQuery(query,null);
    }

    public Cursor getDinerusList(){
        return runRawQuery(DBConstants.Queries.QUERY_DINERUS_SELECT_ALL);
    }

    public void insertOneDinerus(DinerusData dinerusData) throws JSONException {
        String query = DBConstants.Queries.QUERY_DINERUS_INSERT_ONE;

        query = query.replace(DBConstants.Tag.dinerusDate, getStringForQuery(DateUtils.formatDate(dinerusData.getDinerusDate())));
        query = query.replace(DBConstants.Tag.dinerusCategory, dinerusData.getDinerusCategory().getCategoryId().toString());
        query = query.replace(DBConstants.Tag.dinerusWallet, dinerusData.getDinerusWallet().getWalletId().toString());
        query = query.replace(DBConstants.Tag.dinerusDetail, getStringForQuery(dinerusData.getDinerusDetail()));
        query = query.replace(DBConstants.Tag.dinerusAmountDetail, getStringForQuery(dinerusData.getDinerusDataDetailAsJSON().toString()));
        database.execSQL(query);
    }

    public void updateOneDinerus(DinerusData dinerusData) throws JSONException {
        String query = DBConstants.Queries.QUERY_DINERUS_UPDATE_ONE;
        query = query.replace(DBConstants.Tag.dinerusId, dinerusData.getDinerusId().toString());
        query = query.replace(DBConstants.Tag.dinerusDate, getStringForQuery(DateUtils.formatDate(dinerusData.getDinerusDate())));
        query = query.replace(DBConstants.Tag.dinerusCategory, dinerusData.getDinerusCategory().getCategoryId().toString());
        query = query.replace(DBConstants.Tag.dinerusWallet, dinerusData.getDinerusWallet().getWalletId().toString());
        query = query.replace(DBConstants.Tag.dinerusDetail, getStringForQuery(dinerusData.getDinerusDetail()));
        query = query.replace(DBConstants.Tag.dinerusAmountDetail, getStringForQuery(dinerusData.getDinerusDataDetailAsJSON().toString()));

        database.execSQL(query);
    }

    public void deleteOneDinerus(DinerusData dinerusData){
        String query = DBConstants.Queries.QUERY_DINERUS_REMOVE_ONE;
        query = query.replace(DBConstants.Tag.dinerusId, dinerusData.getDinerusId().toString());
        database.execSQL(query);
    }

    public Cursor getCategoryList(){
        return runRawQuery(DBConstants.Queries.QUERY_CATEGORY_SELECT_ALL);
    }

    public void insertOneCategory(CategoryData categoryData) throws JSONException {
        String query = DBConstants.Queries.QUERY_CATEGORY_INSERT_ONE;

        query = query.replace(DBConstants.Tag.categoryName, getStringForQuery(categoryData.getCategoryName()));
        query = query.replace(DBConstants.Tag.categoryDescription, getStringForQuery(categoryData.getCategoryDescription()));
        database.execSQL(query);
    }

    public void updateOneCategory(CategoryData categoryData) throws JSONException {
        String query = DBConstants.Queries.QUERY_CATEGORY_UPDATE_ONE;
        query = query.replace(DBConstants.Tag.categoryId, categoryData.getCategoryId().toString());
        query = query.replace(DBConstants.Tag.categoryName, getStringForQuery(categoryData.getCategoryName()));
        query = query.replace(DBConstants.Tag.categoryDescription, getStringForQuery(categoryData.getCategoryDescription()));

        database.execSQL(query);
    }

    public void deleteOneCategory(CategoryData categoryData){
        String query = DBConstants.Queries.QUERY_CATEGORY_REMOVE_ONE;
        query = query.replace(DBConstants.Tag.categoryId, categoryData.getCategoryId().toString());
        database.execSQL(query);
    }

    public Cursor getWalletList(){
        return runRawQuery(DBConstants.Queries.QUERY_WALLET_SELECT_ALL);
    }

    public void insertOneWallet(WalletData walletData) throws JSONException {
        String query = DBConstants.Queries.QUERY_WALLET_INSERT_ONE;

        query = query.replace(DBConstants.Tag.walletName, getStringForQuery(walletData.getWalletName()));
        query = query.replace(DBConstants.Tag.walletDescription, getStringForQuery(walletData.getWalletDescription()));
        database.execSQL(query);
    }

    public void updateOneWallet(WalletData walletData) throws JSONException {
        String query = DBConstants.Queries.QUERY_WALLET_UPDATE_ONE;
        query = query.replace(DBConstants.Tag.walletId, walletData.getWalletId().toString());
        query = query.replace(DBConstants.Tag.walletName, getStringForQuery(walletData.getWalletName()));
        query = query.replace(DBConstants.Tag.walletDescription, getStringForQuery(walletData.getWalletDescription()));

        database.execSQL(query);
    }

    public void deleteOneWallet(WalletData walletData){
        String query = DBConstants.Queries.QUERY_WALLET_REMOVE_ONE;
        query = query.replace(DBConstants.Tag.walletId, walletData.getWalletId().toString());
        database.execSQL(query);
    }





    private String getStringForQuery(String value){
        return "'".concat(value).concat("'");
    }
    private String getStringForQuery(long value){
        return "'".concat(String.valueOf(value)).concat("'");
    }
}
