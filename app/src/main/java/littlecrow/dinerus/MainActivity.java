package littlecrow.dinerus;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import org.json.JSONException;

import littlecrow.dinerus.data.LoadData;

public class MainActivity extends AppCompatActivity {

    ProgressBar progressLoadingData;
    FloatingActionButton floatingActionButtonAddDinerus;
    Context context = this;

    DinerusReport dinerusReport = new DinerusReport();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressLoadingData =findViewById(R.id.activity_main_progressbar_loading_data);

        floatingActionButtonAddDinerus = findViewById(R.id.activity_main_floating_button_add_dinerus);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_dinerus_report, dinerusReport)
                .commit();

        floatingActionButtonAddDinerus.setOnClickListener(v -> {
            Intent i = new Intent(v.getContext(), DinerusEditorActivity.class);
            startActivityForResult(i, 1);
        });

        loadData();

    }

    public void loadData(){
        progressLoadingData.setVisibility(View.VISIBLE);
        try {
            LoadData.INSTANCE.loadData(context);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        progressLoadingData.setVisibility(View.INVISIBLE);
    }

}
