package littlecrow.dinerus.data;

import android.database.Cursor;

public class WalletData {
    private Integer walletId;
    private String walletName;
    private String walletDescription;

    public WalletData(Cursor cursorWallet){
        this.walletId = cursorWallet.getInt(0);
        this.walletName = cursorWallet.getString(1);
        this.walletDescription = cursorWallet.getString(2);
    }

    public Integer getWalletId() {
        return walletId;
    }

    public void setWalletId(Integer walletId) {
        this.walletId = walletId;
    }

    public String getWalletName() {
        return walletName;
    }

    public void setWalletName(String walletName) {
        this.walletName = walletName;
    }

    public String getWalletDescription() {
        return walletDescription;
    }

    public void setWalletDescription(String walletDescription) {
        this.walletDescription = walletDescription;
    }
}
