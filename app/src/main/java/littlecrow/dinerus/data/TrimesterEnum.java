package littlecrow.dinerus.data;

public enum TrimesterEnum  {
    T1(1, "T1"),
    T2(2, "T2"),
    T3(3, "T3"),
    T4(4, "T4"),
    WTF(-1, "wtf");


    private int number;
    private String name;


    TrimesterEnum(Integer number, String name) {
        this.number = number;
        this.name = name;
    }

    public String toString(){
        return name;
    }

    public Integer getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public static TrimesterEnum getTrimesterEnum(Integer number) {
        switch (number) {
            case 0:
            case 1:
            case 2:
                return T1;
            case 3:
            case 4:
            case 5:
                return T2;
            case 6:
            case 7:
            case 8:
                return T3;
            case 9:
            case 10:
            case 11:
                return T4;
            default:
                return WTF;
        }

    }
}
