package littlecrow.dinerus.data;

import java.math.BigDecimal;

public enum DinerusType {
    /*INCOME("income", BigDecimal::add),
    MANDATORY_OUTCOME("mandatory_outcome", BigDecimal::subtract),
    PERSONAL_OUTCOME("personal_outcome", BigDecimal::subtract);

    private String name;
    private Function<String,Boolean> function;
    private BiFunction<BigDecimal, BigDecimal, BigDecimal> dinerusTypeOperation;

    DinerusType(String name, BiFunction<BigDecimal, BigDecimal, BigDecimal> dinerusTypeOperation){
        this.name = name;
        this.function = name :: equals;
        this.dinerusTypeOperation = dinerusTypeOperation;
    }

    public String getName() {
        return name;
    }

    public BiFunction<BigDecimal, BigDecimal, BigDecimal> getDinerusTypeOperation() {
        return dinerusTypeOperation;
    }

    public static DinerusType getDinerusType(String name){
            return Arrays.stream(DinerusType.values()).filter((entity) -> entity.function.apply(name)).findAny().orElse(null);
    }

    */

    INCOME("income"),
    OUTCOME("outcome"),
    MANDATORY_OUTCOME("mandatory_outcome"),
    PERSONAL_OUTCOME("personal_outcome");

    private String name;

    DinerusType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public static DinerusType getDinerusType(String name) {
        switch (name) {
            case "income":
                return INCOME;
            case "outcome":
                return OUTCOME;
            case "mandatory_outcome":
                return MANDATORY_OUTCOME;
            case "personal_outcome":
                return PERSONAL_OUTCOME;
            default:
                return null;
        }
    }

    public static BigDecimal calculateNewAmount(BigDecimal actualAmount, BigDecimal newAmount, DinerusType dinerusType){
        switch (dinerusType.getName()) {
            case "income":
                return actualAmount.add(newAmount);
            case "outcome":
                return actualAmount.subtract(newAmount);
            case "mandatory_outcome":
                return actualAmount.subtract(newAmount);
            case "personal_outcome":
                return actualAmount.subtract(newAmount);
            default:
                return actualAmount;
        }
    }
}
