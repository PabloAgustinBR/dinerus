package littlecrow.dinerus.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DinerusCollection extends DinerusObject{

    Object identifier;

    Map<Object,DinerusCollection> dinerusCollectionMap = new HashMap<>();

    List<DinerusObject> dinerusObjectList = new ArrayList<>();

    public DinerusCollection(Object identifier){
        this.identifier = identifier;
    }

    public List<DinerusObject> getDinerusObjectList() {
        return dinerusObjectList;
    }

    public void setDinerusObjectList(List<DinerusObject> dinerusObjectList) {
        this.dinerusObjectList = dinerusObjectList;
    }

    public void addDinerusObject(DinerusObject dinerusObject) {
        this.dinerusObjectList.add(dinerusObject);
    }

    public Object getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Object identifier) {
        this.identifier = identifier;
    }

    public DinerusCollection getDinerusCollection(Object identifier) {
        DinerusCollection dinerusCollection = dinerusCollectionMap.get(identifier);
        if(dinerusCollection == null){
            dinerusCollection = new DinerusCollection(identifier);
            dinerusCollectionMap.put(identifier,dinerusCollection);
            dinerusObjectList.add(dinerusCollection);
        }
        return dinerusCollection;
    }


    public void putDinerusDetailAmount(DinerusData dinerusData) {
        for (DinerusDataDetail dinerusDataDetail : dinerusData.getDinerusDataDetailList()){
            super.putDinerusDetailAmount(dinerusDataDetail.getDinerusCurrency(), dinerusDataDetail.getDinerusType(), dinerusDataDetail.getDinerusAmount());
        }
    }

}
