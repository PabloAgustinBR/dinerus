package littlecrow.dinerus.data;

import android.util.SparseArray;

import org.json.JSONException;

import java.util.HashSet;
import java.util.Set;

public class AccessData {

    private static DinerusCollection totalData = new DinerusCollection("total_data");

    private static SparseArray<CategoryData> categoryDataList = new SparseArray<>();
    private static SparseArray<WalletData> walletDataList = new SparseArray<>();
    private static SparseArray<DinerusData> dinerusDataList = new SparseArray<>();

    private static Set<CurrencyEnum> dinerusCurrencies = new HashSet<>();

    public static DinerusCollection getTotalData(){
        return totalData;
    }

    public static void resetAccessData(){
        totalData = new DinerusCollection("total_data");
        categoryDataList = new SparseArray<>();
        walletDataList = new SparseArray<>();
        dinerusDataList = new SparseArray<>();
    }

    public static void loadData() throws JSONException {
        LoadData.INSTANCE.loadDinerusData();
    }


    public static SparseArray<CategoryData> getCategoryList() {
        return categoryDataList;
    }

    public static CategoryData getCategory(Integer categoryId) {
        return categoryDataList.get(categoryId);
    }

    public static void addDinerusCategory(CategoryData dinerusCategory) {
        categoryDataList.put(dinerusCategory.getCategoryId(),dinerusCategory);
    }

    public static SparseArray<WalletData> getWalletList() {
        return walletDataList;
    }

    public static WalletData getWallet(Integer walletId) {
        return walletDataList.get(walletId);
    }

    public static void addDinerusWallet(WalletData dinerusWallet) {
        walletDataList.put(dinerusWallet.getWalletId(),dinerusWallet);
    }

    public static SparseArray<DinerusData> getDinerusList() {
        return dinerusDataList;
    }

    public static DinerusData getDinerus(Integer dinerusId) {
        return dinerusDataList.get(dinerusId);
    }

    public static void addDinerusDinerus(DinerusData dinerusDinerus) {
        dinerusDataList.put(dinerusDinerus.getDinerusId(),dinerusDinerus);
    }


    public static void addDinerusCurrency(CurrencyEnum dinerusCurrency) {
        dinerusCurrencies.add(dinerusCurrency);
    }

}
