package littlecrow.dinerus.data;

public enum SemesterEnum  {
    S1(1, "S1"),
    S2(2, "S2"),
    WTF(-1, "wtf");


    private int number;
    private String name;


    SemesterEnum(Integer number, String name) {
        this.number = number;
        this.name = name;
    }

    public String toString(){
        return name;
    }

    public Integer getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public static SemesterEnum getSemesterEnum(Integer number) {
        switch (number) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                return S1;
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
                return S2;
            default:
                return WTF;
        }

    }
}
