package littlecrow.dinerus.data;

import android.database.Cursor;

public class CategoryData {
    private Integer categoryId;
    private String categoryName;
    private String categoryDescription;

    public CategoryData(Cursor cursorCategory){
        this.categoryId = cursorCategory.getInt(0);
        this.categoryName = cursorCategory.getString(1);
        this.categoryDescription = cursorCategory.getString(2);
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public void setCategoryDescription(String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }
}
