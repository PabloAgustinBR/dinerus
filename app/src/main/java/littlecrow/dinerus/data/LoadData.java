package littlecrow.dinerus.data;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.SparseArray;

import org.json.JSONException;

import java.io.IOException;
import java.util.Calendar;

import littlecrow.dinerus.datasource.DinerusDataSource;

public enum LoadData {

    INSTANCE;

    DinerusDataSource dataSource;

    public void loadData(Context context) throws JSONException {
        dataSource = DinerusDataSource.INSTANCE;
        try {
            dataSource.init(context);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        loadDinerusData();
    }

    public void loadDinerusData() throws JSONException {
        AccessData.resetAccessData();
        DinerusCollection totalData = AccessData.getTotalData();

        Cursor cursorCategories= dataSource.getCategoryList();
        while(cursorCategories.moveToNext()) {
            AccessData.addDinerusCategory(new CategoryData(cursorCategories));
        }

        Cursor cursorWallet= dataSource.getWalletList();
        while(cursorWallet.moveToNext()) {
            AccessData.addDinerusWallet(new WalletData(cursorWallet));
        }

        Cursor cursorDinerus= dataSource.getDinerusList();
        while(cursorDinerus.moveToNext()) {
            DinerusData dinerusData = new DinerusData(cursorDinerus);
            Calendar dinerusDataDate = Calendar.getInstance();
            dinerusDataDate.setTime(dinerusData.getDinerusDate());

            DinerusCollection yearData = totalData.getDinerusCollection(dinerusDataDate.get(Calendar.YEAR));

            DinerusCollection semesterData = yearData.getDinerusCollection(SemesterEnum.getSemesterEnum(dinerusDataDate.get(Calendar.MONTH)));

            DinerusCollection trimesterData = semesterData.getDinerusCollection(TrimesterEnum.getTrimesterEnum(dinerusDataDate.get(Calendar.MONTH)));

            DinerusCollection monthData = trimesterData.getDinerusCollection(MonthEnum.getMonthEnum(dinerusDataDate.get(Calendar.MONTH)));

            SparseArray<DayOfWeekEnum> dayKey = new SparseArray<>();
            dayKey.put(dinerusDataDate.get(Calendar.DAY_OF_MONTH),DayOfWeekEnum.getDayOfWeekEnum(dinerusDataDate.get(Calendar.DAY_OF_WEEK)));
            DinerusCollection dayData = monthData.getDinerusCollection(dayKey);

            dayData.addDinerusObject(dinerusData);

            dayData.putDinerusDetailAmount(dinerusData);
            monthData.putDinerusDetailAmount(dinerusData);
            yearData.putDinerusDetailAmount(dinerusData);
            totalData.putDinerusDetailAmount(dinerusData);

            AccessData.addDinerusDinerus(dinerusData);
            AccessData.addDinerusCategory(dinerusData.getDinerusCategory());

        }
    }
}
