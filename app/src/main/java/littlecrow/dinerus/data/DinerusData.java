package littlecrow.dinerus.data;

import android.database.Cursor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DinerusData extends DinerusObject {
    private Integer dinerusId;
    private Date dinerusDate;
    private CategoryData dinerusCategory;
    private WalletData dinerusWallet;
    private String dinerusDetail;
    private List<DinerusDataDetail> dinerusDataDetailList = new ArrayList<>();

    DinerusData(Cursor dinerusCursor) throws JSONException {

        this.dinerusId = dinerusCursor.getInt(0);
        this.dinerusDate = DateUtils.parseDate(dinerusCursor.getString(1));
        this.dinerusCategory = AccessData.getCategory(dinerusCursor.getInt(2));
        this.dinerusWallet = AccessData.getWallet(dinerusCursor.getInt(3));
        this.dinerusDetail = dinerusCursor.getString(4);
        JSONArray amountDetails = new JSONArray(dinerusCursor.getString(5));
        for (int i = 0; i < amountDetails.length(); i++) {
            DinerusDataDetail dinerusDataDetail = new DinerusDataDetail(amountDetails.getJSONObject(i));
            dinerusDataDetailList.add(dinerusDataDetail);
            putDinerusDetailAmount(dinerusDataDetail.getDinerusCurrency(), dinerusDataDetail.getDinerusType(), dinerusDataDetail.getDinerusAmount());
            AccessData.addDinerusCurrency(dinerusDataDetail.getDinerusCurrency());
        }
    }

    public DinerusData(){

    }

    public Integer getDinerusId() {
        return dinerusId;
    }

    public void setDinerusId(Integer dinerusId) {
        this.dinerusId = dinerusId;
    }

    public Date getDinerusDate() {
        return dinerusDate;
    }

    public void setDinerusDate(Date dinerusDate) {
        this.dinerusDate = dinerusDate;
    }

    public CategoryData getDinerusCategory() {
        return dinerusCategory;
    }

    public void setDinerusCategory(CategoryData dinerusCategory) {
        this.dinerusCategory = dinerusCategory;
    }

    public WalletData getDinerusWallet() {
        return dinerusWallet;
    }

    public void setDinerusWallet(WalletData dinerusWallet) {
        this.dinerusWallet = dinerusWallet;
    }

    public String getDinerusDetail() {
        return dinerusDetail;
    }

    public void setDinerusDetail(String dinerusDetail) {
        this.dinerusDetail = dinerusDetail;
    }

    public List<DinerusDataDetail> getDinerusDataDetailList() {
        return dinerusDataDetailList;
    }

    public JSONArray getDinerusDataDetailAsJSON() throws JSONException {
        JSONArray jsonArray = new JSONArray();
        for(DinerusDataDetail dinerusDataDetail : dinerusDataDetailList){
            JSONObject dinerusDetail = new JSONObject();
            dinerusDetail.put("detail_name", dinerusDataDetail.getDinerusName());
            dinerusDetail.put("detail_type", dinerusDataDetail.getDinerusType().getName());
            dinerusDetail.put("detail_currency", dinerusDataDetail.getDinerusCurrency().getName());
            dinerusDetail.put("detail_amount", dinerusDataDetail.getDinerusAmount());
            jsonArray.put(dinerusDetail);
        }

        return jsonArray;
    }

    public void setDinerusDataDetailList(List<DinerusDataDetail> dinerusDataDetailList) {
        this.dinerusDataDetailList = dinerusDataDetailList;
    }
}
