package littlecrow.dinerus.data;

public enum MonthEnum {
    JANUARY(0, "january"),
    FEBRUARY(1, "february"),
    MARCH(2, "march"),
    APRIL(3, "april"),
    MAY(4, "may"),
    JUNE(5, "june"),
    JULY(6, "july"),
    AUGUST(7, "august"),
    SEPTEMBER(8, "september"),
    OCTOBER(9, "october"),
    NOVEMBER(10, "november"),
    DECEMBER(11, "december"),
    WTF(-1, "wtf");


    private int number;
    private String name;


    MonthEnum(Integer number, String name) {
        this.number = number;
        this.name = name;
    }

    public String toString(){
        return name;
    }

    public Integer getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public static MonthEnum getMonthEnum(Integer number) {
        switch (number) {
            case 0:
                return JANUARY;
            case 1:
                return FEBRUARY;
            case 2:
                return MARCH;
            case 3:
                return APRIL;
            case 4:
                return MAY;
            case 5:
                return JUNE;
            case 6:
                return JULY;
            case 7:
                return AUGUST;
            case 8:
                return SEPTEMBER;
            case 9:
                return OCTOBER;
            case 10:
                return NOVEMBER;
            case 11:
                return DECEMBER;
            default:
                return WTF;
        }

    }
}
