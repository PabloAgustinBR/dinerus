package littlecrow.dinerus.data;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;

public class DinerusDataDetail extends DinerusObject {
    private String dinerusName;
    private DinerusType dinerusType;
    private BigDecimal dinerusAmount;
    private CurrencyEnum dinerusCurrency;

    public DinerusDataDetail(){

    }

    DinerusDataDetail(JSONObject dinerusDataDetail) throws JSONException {
        this.dinerusName = dinerusDataDetail.optString("detail_name");
        this.dinerusType = DinerusType.getDinerusType(dinerusDataDetail.getString("detail_type"));
        this.dinerusCurrency = CurrencyEnum.getCurrencyEnum(dinerusDataDetail.getString("detail_currency"));
        this.dinerusAmount = new BigDecimal(dinerusDataDetail.getString("detail_amount"));

    }

    public String getDinerusName() {
        return dinerusName;
    }

    public void setDinerusName(String dinerusName) {
        this.dinerusName = dinerusName;
    }

    public DinerusType getDinerusType() {
        return dinerusType;
    }

    public void setDinerusType(DinerusType dinerusType) {
        this.dinerusType = dinerusType;
    }

    public BigDecimal getDinerusAmount() {
        return dinerusAmount;
    }

    public void setDinerusAmount(BigDecimal dinerusAmount) {
        this.dinerusAmount = dinerusAmount;
    }

    public CurrencyEnum getDinerusCurrency() {
        return dinerusCurrency;
    }

    public void setDinerusCurrency(CurrencyEnum dinerusCurrency) {
        this.dinerusCurrency = dinerusCurrency;
    }
}
