package littlecrow.dinerus.data;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

public abstract class DinerusObject {

    private Map<CurrencyEnum,DinerusAmountDetail> dinerusAmountDetailMap = new HashMap<>();

    public Map<CurrencyEnum,DinerusAmountDetail> getDinerusAmountDetailMap(){
        return dinerusAmountDetailMap;
    }

    public DinerusAmountDetail getDinerusAmountDetail(CurrencyEnum currencyEnum){
        return dinerusAmountDetailMap.get(currencyEnum);
    }

    public void putDinerusDetailAmount(CurrencyEnum currencyEnum, DinerusType dinerusType, BigDecimal amount) {

        DinerusAmountDetail dinerusAmountDetail = this.dinerusAmountDetailMap.get(currencyEnum);

        if(dinerusAmountDetail == null){
            dinerusAmountDetail = new DinerusAmountDetail();
            this.dinerusAmountDetailMap.put(currencyEnum, dinerusAmountDetail);
        }
        dinerusAmountDetail.putDinerusDetailAmount(dinerusType,amount);

    }

    public class DinerusAmountDetail {
        BigDecimal dinerusAmount = new BigDecimal(BigInteger.ZERO);
        Map<DinerusType, BigDecimal> dinerusAmountByType = new HashMap<>();

        public void putDinerusDetailAmount(DinerusType dinerusType, BigDecimal amount) {
            BigDecimal previousAmount = this.dinerusAmountByType.get(dinerusType);

            this.dinerusAmountByType.put(dinerusType, previousAmount != null ? previousAmount.add(amount) : amount);

            this.dinerusAmount = DinerusType.calculateNewAmount(this.dinerusAmount,amount,dinerusType);
        }

        public BigDecimal getDinerusAmount() {
            return dinerusAmount;
        }

        public Map<DinerusType, BigDecimal> getDinerusAmountByTypeMap() {
            return dinerusAmountByType;
        }

        public BigDecimal getDinerusAmountByType(DinerusType dinerusType) {
            return dinerusAmountByType.get(dinerusType);
        }
    }

}
