package littlecrow.dinerus.data;

public enum CurrencyEnum {
    ARS("ARS","$"),
    USD("USD","$"),
    EUR("EUR","€"),
    BRL("BRL","R$"),
    CLP("CLP","$"),
    JPY("JPY","¥"),
    WTF("WTF", "wtf");

    private String currencyName;
    private String currencyCode;
    private String currencySymbol;

    CurrencyEnum(String currencyName, String currencySymbol){
        this.currencyName = currencyName;
        this.currencyCode = "currency_".concat(currencyName.toLowerCase());
        this.currencySymbol = currencySymbol;
    }

    public String getName(){
        return this.currencyName;
    }

    public static CurrencyEnum getCurrencyEnum(String currencyName){
        switch (currencyName){
            case "ARS":
                return ARS;
            case "USD":
                return USD;
            case "EUR":
                return EUR;
            case "BRL":
                return BRL;
            case "CLP":
                return CLP;
            case "JPY":
                return JPY;
            default:
                return WTF;
        }
    }
}
