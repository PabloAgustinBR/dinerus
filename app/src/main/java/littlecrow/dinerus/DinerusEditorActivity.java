package littlecrow.dinerus;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.SQLException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONException;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import littlecrow.dinerus.data.CurrencyEnum;
import littlecrow.dinerus.data.DinerusData;
import littlecrow.dinerus.data.DinerusDataDetail;
import littlecrow.dinerus.data.DinerusType;
import littlecrow.dinerus.datasource.DinerusDataSource;

public class DinerusEditorActivity extends AppCompatActivity {


    private Button dinerusEditorButtonAccept;
    private Button dinerusEditorButtonMultiAmount;

    private Boolean multiAmount = false;

    DinerusDataSource dataSource;

    private LinearLayout linearLayoutDinerusSingleAmount;
    private LinearLayout linearLayoutDinerusMultiAmount;
    private LinearLayout linearLayoutDinerusMultiAmountList;


    private RadioButton radioButtonDinerusSingleIncome;
    private RadioButton radioButtonDinerusSingleOutcome;
    private Spinner spinnerDinerusSingleCurrencySpinner;

    private EditText editTextDinerusSingleAmount;

    private Button buttonAddAmountDetail;

    List<String> currenciesList= Arrays.asList("ARS","USD");
    ArrayAdapter<String> spinnerAdapterCurrencyList;

    EditText category;
    EditText detail;
    EditText amount;

    private Integer detailListId = 0;
    private Integer easyCountDetailsReady = 0;

    SparseArray<MultiAmountDetailView> multiAmountDetailViewMap = new SparseArray<>();

    private DinerusData dinerusData;
    private List<DinerusDataDetail> dinerusDataDetailList = new ArrayList<>();
    private Date dinerusDataDate = new Date();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dinerus_editor);

        dataSource = DinerusDataSource.INSTANCE;
        try {
            dataSource.init(this);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        dinerusEditorButtonAccept = findViewById(R.id.dinerus_editor_button_accept);
        dinerusEditorButtonMultiAmount = findViewById(R.id.dinerus_editor_button_enable_list);
        dinerusEditorButtonMultiAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                multiAmount = !multiAmount;
                loadContentAmountLayout();
            }
        });

        linearLayoutDinerusSingleAmount = findViewById(R.id.dinerus_editor_single_amount_linear_layout);
        linearLayoutDinerusMultiAmount = findViewById(R.id.dinerus_editor_multi_amount_linear_layout);
        linearLayoutDinerusMultiAmountList = findViewById(R.id.dinerus_editor_multi_amount_linear_layout_amount_list);
        buttonAddAmountDetail = findViewById(R.id.dinerus_editor_multi_amount_button_add_new);
        buttonAddAmountDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewMultiAmount(v);
            }
        });

        spinnerAdapterCurrencyList = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, currenciesList);

        editTextDinerusSingleAmount = findViewById(R.id.dinerus_editor_single_amount_edit_text_amount);
        radioButtonDinerusSingleIncome = findViewById(R.id.dinerus_editor_single_amount_radio_button_income);
        radioButtonDinerusSingleOutcome = findViewById(R.id.dinerus_editor_single_amount_radio_button_outcome);

        spinnerDinerusSingleCurrencySpinner = findViewById(R.id.dinerus_editor_single_amount_spinner_currency);
        spinnerDinerusSingleCurrencySpinner.setAdapter(spinnerAdapterCurrencyList);


        category = findViewById(R.id.category);
        detail = findViewById(R.id.detail);

        dinerusEditorButtonAccept.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        // Crear Cuenta...
                        //dismiss();

                        DinerusData newDinerusData = new DinerusData();
                        newDinerusData.setDinerusCategory(null);
                        newDinerusData.setDinerusDetail(detail.getText().toString());
                        newDinerusData.setDinerusDataDetailList(getDinerusDataDetailList());
                        newDinerusData.setDinerusDate(new Date());

                        try {
                            dataSource.insertOneDinerus(newDinerusData);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        finish();

                    }
                }
        );

        loadContentAmountLayout();

    }


    public void finish(){

        setResult(RESULT_OK);

        super.finish();

    }
    public void setDinerusData(DinerusData dinerusData){
        this.dinerusData = dinerusData;
    }

    public void loadContentAmountLayout(){

        if(multiAmount){
            linearLayoutDinerusSingleAmount.setVisibility(View.GONE);
            linearLayoutDinerusMultiAmount.setVisibility(View.VISIBLE);
            dinerusEditorButtonMultiAmount.setText("Stop multi amount");
        }else{
            linearLayoutDinerusSingleAmount.setVisibility(View.VISIBLE);
            linearLayoutDinerusMultiAmount.setVisibility(View.GONE);
            dinerusEditorButtonMultiAmount.setText("Start multi amount");

        }

    }

    public List<DinerusDataDetail> getDinerusDataDetailList(){
        List<DinerusDataDetail> dinerusDataDetailList = new ArrayList<>();
        if(multiAmount){
            for (int i = 0; i < multiAmountDetailViewMap.size(); i++) {
                MultiAmountDetailView multiAmountDetailView = multiAmountDetailViewMap.get(i);
                DinerusDataDetail dinerusDataDetail = new DinerusDataDetail();
                dinerusDataDetail.setDinerusAmount(multiAmountDetailView.getDetailAmount());
                dinerusDataDetail.setDinerusName(multiAmountDetailView.getDetailName());
                dinerusDataDetail.setDinerusCurrency(multiAmountDetailView.getDetailCurrencyEnum());
                dinerusDataDetail.setDinerusType(multiAmountDetailView.getDetailDinerusType());

                dinerusDataDetailList.add(dinerusDataDetail);
            }
        }else{
            DinerusDataDetail dinerusDataDetail = new DinerusDataDetail();
            dinerusDataDetail.setDinerusAmount(new BigDecimal(editTextDinerusSingleAmount.getText().toString()));
            dinerusDataDetail.setDinerusCurrency(CurrencyEnum.getCurrencyEnum(spinnerDinerusSingleCurrencySpinner.getSelectedItem().toString()));
            dinerusDataDetail.setDinerusType(radioButtonDinerusSingleIncome.isChecked() ? DinerusType.INCOME : DinerusType.OUTCOME);
            dinerusDataDetailList.add(dinerusDataDetail);
        }
        return dinerusDataDetailList;
    }


    private void addNewMultiAmount(View v){
        MultiAmountDetailView multiAmountDetailView = new MultiAmountDetailView();
        Integer detailId = getNextDetailListId();//getNextDetailListId();

        multiAmountDetailViewMap.append(detailId,multiAmountDetailView);
        LinearLayout detailLinearLayout = new LinearLayout(v.getContext());
        detailLinearLayout.setOrientation(LinearLayout.VERTICAL);
        multiAmountDetailView.setDetailLinearLayout(detailLinearLayout);


        linearLayoutDinerusMultiAmountList.addView(detailLinearLayout);


        LayoutInflater inflater = (LayoutInflater) v.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        LinearLayout closeLinearLayout = (LinearLayout) inflater.inflate(R.layout.dinerus_editor_multi_amount_close, null);
        closeLinearLayout.setVisibility(View.GONE);
        closeLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                multiAmountDetailView.openEditable();
            }
        });
        multiAmountDetailView.setCloseLinearLayout(closeLinearLayout);
        detailLinearLayout.addView(closeLinearLayout);

        TextView closeTextViewId = closeLinearLayout.findViewById(R.id.dinerus_editor_multi_amount_close_text_view_id);
        closeTextViewId.setText(String.valueOf(detailId));
        multiAmountDetailView.setCloseTextViewId(closeTextViewId);

        TextView closeTextViewName = closeLinearLayout.findViewById(R.id.dinerus_editor_multi_amount_close_text_view_name);
        multiAmountDetailView.setCloseTextViewName(closeTextViewName);

        TextView closeTextViewAmount = closeLinearLayout.findViewById(R.id.dinerus_editor_multi_amount_close_text_view_amount);
        multiAmountDetailView.setCloseTextViewAmount(closeTextViewAmount);

        TextView closeTextViewCurrency = closeLinearLayout.findViewById(R.id.dinerus_editor_multi_amount_close_text_view_currency);
        multiAmountDetailView.setCloseTextViewCurrency(closeTextViewCurrency);

        TextView closeTextViewType = closeLinearLayout.findViewById(R.id.dinerus_editor_multi_amount_close_text_view_dinerus_type);
        multiAmountDetailView.setCloseTextViewType(closeTextViewType);


        LinearLayout openLinearLayout = (LinearLayout) inflater.inflate(R.layout.dinerus_editor_multi_amount_open, null);
        multiAmountDetailView.setOpenLinearLayout(openLinearLayout);
        detailLinearLayout.addView(openLinearLayout);

        TextView openTextViewId = openLinearLayout.findViewById(R.id.dinerus_editor_multi_amount_open_text_view_id);
        openTextViewId.setText(String.valueOf(detailId));
        multiAmountDetailView.setOpenTextViewId(openTextViewId);

        EditText openEditTextName = openLinearLayout.findViewById(R.id.dinerus_editor_multi_amount_open_edit_text_name);
        multiAmountDetailView.setOpenEditTextName(openEditTextName);

        EditText openEditTextAmount = openLinearLayout.findViewById(R.id.dinerus_editor_multi_amount_open_edit_text_amount);
        multiAmountDetailView.setOpenEditTextAmount(openEditTextAmount);

        Spinner openSpinnerCurrency = openLinearLayout.findViewById(R.id.dinerus_editor_multi_amount_open_sppiner_currency);
        openSpinnerCurrency.setAdapter(spinnerAdapterCurrencyList);
        multiAmountDetailView.setOpenSpinnerCurrency(openSpinnerCurrency);

        RadioButton openRadioButtonIncome = openLinearLayout.findViewById(R.id.dinerus_editor_multi_amount_open_radio_button_income);
        openRadioButtonIncome.setChecked(true);
        multiAmountDetailView.setOpenRadioButtonIncome(openRadioButtonIncome);

        RadioButton openRadioButtonOutcome = openLinearLayout.findViewById(R.id.dinerus_editor_multi_amount_open_radio_button_outcome);
        multiAmountDetailView.setOpenRadioButtonOutcome(openRadioButtonOutcome);


        Button openButtonAccept = openLinearLayout.findViewById(R.id.dinerus_editor_multi_amount_open_button_accept);
        openButtonAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                multiAmountDetailView.updateValues();
            }
        });
        Button openButtonDelete = openLinearLayout.findViewById(R.id.dinerus_editor_multi_amount_open_button_delete);
        openButtonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteOneDetail(detailId, detailLinearLayout);
            }
        });
    }

    private Integer getNextDetailListId(){
        return detailListId++;
    }

    private void deleteOneDetail(Integer detailId, LinearLayout detailLinearLayout){
        multiAmountDetailViewMap.delete(detailId);
        linearLayoutDinerusMultiAmountList.removeView(detailLinearLayout);
    }






    private class MultiAmountDetailView{
        private LinearLayout detailLinearLayout;
        private TextView detailTextViewId;

        private LinearLayout openLinearLayout;
        private TextView openTextViewId;
        private EditText openEditTextName;
        private EditText openEditTextAmount;
        private Spinner openSpinnerCurrency;
        private RadioButton openRadioButtonIncome;
        private RadioButton openRadioButtonOutcome;

        private LinearLayout closeLinearLayout;
        private TextView closeTextViewId;
        private TextView closeTextViewName;
        private TextView closeTextViewAmount;
        private TextView closeTextViewCurrency;
        private TextView closeTextViewType;


        private String detailName;
        private BigDecimal detailAmount;
        private CurrencyEnum detailCurrencyEnum;
        private DinerusType detailDinerusType;

        public void updateValues(){

            detailName = openEditTextName.getText().toString();
            detailAmount = new BigDecimal(openEditTextAmount.getText().toString());
            detailCurrencyEnum = CurrencyEnum.getCurrencyEnum(openSpinnerCurrency.getSelectedItem().toString());
            detailDinerusType = openRadioButtonIncome.isChecked() ? DinerusType.INCOME : DinerusType.OUTCOME;


            closeTextViewName.setText(openEditTextName.getText());
            closeTextViewAmount.setText(openEditTextAmount.getText());
            closeTextViewCurrency.setText(detailCurrencyEnum.getName());
            closeTextViewType.setText(detailDinerusType.getName());
            openLinearLayout.setVisibility(View.GONE);
            closeLinearLayout.setVisibility(View.VISIBLE);


        }

        public void openEditable(){
            openLinearLayout.setVisibility(View.VISIBLE);
            closeLinearLayout.setVisibility(View.GONE);
        }

        public String getDetailName() {
            return detailName;
        }

        public void setDetailName(String detailName) {
            this.detailName = detailName;
        }

        public BigDecimal getDetailAmount() {
            return detailAmount;
        }

        public void setDetailAmount(BigDecimal detailAmount) {
            this.detailAmount = detailAmount;
        }

        public CurrencyEnum getDetailCurrencyEnum() {
            return detailCurrencyEnum;
        }

        public void setDetailCurrencyEnum(CurrencyEnum detailCurrencyEnum) {
            this.detailCurrencyEnum = detailCurrencyEnum;
        }

        public DinerusType getDetailDinerusType() {
            return detailDinerusType;
        }

        public void setDetailDinerusType(DinerusType detailDinerusType) {
            this.detailDinerusType = detailDinerusType;
        }

        public LinearLayout getDetailLinearLayout() {
            return detailLinearLayout;
        }

        public void setDetailLinearLayout(LinearLayout detailLinearLayout) {
            this.detailLinearLayout = detailLinearLayout;
        }

        public TextView getDetailTextViewId() {
            return detailTextViewId;
        }

        public void setDetailTextViewId(TextView detailTextViewId) {
            this.detailTextViewId = detailTextViewId;
        }

        public LinearLayout getOpenLinearLayout() {
            return openLinearLayout;
        }

        public void setOpenLinearLayout(LinearLayout openLinearLayout) {
            this.openLinearLayout = openLinearLayout;
        }

        public TextView getOpenTextViewId() {
            return openTextViewId;
        }

        public void setOpenTextViewId(TextView openTextViewId) {
            this.openTextViewId = openTextViewId;
        }

        public EditText getOpenEditTextName() {
            return openEditTextName;
        }

        public void setOpenEditTextName(EditText openEditTextName) {
            this.openEditTextName = openEditTextName;
        }

        public EditText getOpenEditTextAmount() {
            return openEditTextAmount;
        }

        public void setOpenEditTextAmount(EditText openEditTextAmount) {
            this.openEditTextAmount = openEditTextAmount;
        }

        public Spinner getOpenSpinnerCurrency() {
            return openSpinnerCurrency;
        }

        public void setOpenSpinnerCurrency(Spinner openSpinnerCurrency) {
            this.openSpinnerCurrency = openSpinnerCurrency;
        }

        public RadioButton getOpenRadioButtonIncome() {
            return openRadioButtonIncome;
        }

        public void setOpenRadioButtonIncome(RadioButton openRadioButtonIncome) {
            this.openRadioButtonIncome = openRadioButtonIncome;
        }

        public RadioButton getOpenRadioButtonOutcome() {
            return openRadioButtonOutcome;
        }

        public void setOpenRadioButtonOutcome(RadioButton openRadioButtonOutcome) {
            this.openRadioButtonOutcome = openRadioButtonOutcome;
        }

        public LinearLayout getCloseLinearLayout() {
            return closeLinearLayout;
        }

        public void setCloseLinearLayout(LinearLayout closeLinearLayout) {
            this.closeLinearLayout = closeLinearLayout;
        }

        public TextView getCloseTextViewId() {
            return closeTextViewId;
        }

        public void setCloseTextViewId(TextView closeTextViewId) {
            this.closeTextViewId = closeTextViewId;
        }

        public TextView getCloseTextViewName() {
            return closeTextViewName;
        }

        public void setCloseTextViewName(TextView closeTextViewName) {
            this.closeTextViewName = closeTextViewName;
        }

        public TextView getCloseTextViewAmount() {
            return closeTextViewAmount;
        }

        public void setCloseTextViewAmount(TextView closeTextViewAmount) {
            this.closeTextViewAmount = closeTextViewAmount;
        }

        public TextView getCloseTextViewCurrency() {
            return closeTextViewCurrency;
        }

        public void setCloseTextViewCurrency(TextView closeTextViewCurrency) {
            this.closeTextViewCurrency = closeTextViewCurrency;
        }

        public TextView getCloseTextViewType() {
            return closeTextViewType;
        }

        public void setCloseTextViewType(TextView closeTextViewType) {
            this.closeTextViewType = closeTextViewType;
        }
    }
}
